/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fa.superlemmalist.ejb;

import org.fa.superlemmalist.jpa.LanguageVariant;
import org.fa.superlemmalist.jpa.Lemma;
import org.fa.superlemmalist.jpa.LemmaLink;
import org.fa.superlemmalist.jpa.Superlemma;
import org.fa.superlemmalist.linking.LemmaLinkException;
import org.fa.superlemmalist.linking.LinkHelper;
import org.fa.superlemmalist.linking.NoLemmaFoundException;
import org.fryske_akademy.jpa.EntityInterface;
import org.fryske_akademy.jpa.Param;
import org.fryske_akademy.services.AbstractCrudServiceEnvers;
import org.fryske_akademy.services.CrudWriteService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.annotation.security.DeclareRoles;
import jakarta.annotation.security.RolesAllowed;
import jakarta.ejb.Local;
import jakarta.ejb.Stateless;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.transaction.Transactional;
import java.util.ArrayList;

/**
 *
 * @author eduard
 */
@Local({CrudWriteService.class, ManagingApi.class})
@Stateless
@Transactional
@DeclareRoles({ManagingApi.ROLE})
public class CrudServiceBean extends AbstractCrudServiceEnvers implements ManagingApi {

    private static final Logger LOGGER = LoggerFactory.getLogger(CrudServiceBean.class.getName());

    @PersistenceContext(unitName = "superlemmalist_unit")
    private EntityManager em;

    @Override
    public EntityManager getEntityManager() {
        return em;
    }

    @RolesAllowed({ManagingApi.ROLE})
    @Override
    public LemmaLink createSuperlemma(String lemma, String linguistics, String meaning) {
        Superlemma s = new Superlemma();
        s.setCode(lemma);
        s.setPos(linguistics);
        s.setMeaning(meaning);
        Lemma l = new Lemma();
        l.setLemma(lemma);
        l.setLanguageVariant(findOne("LanguageVariant.byCode", Param.one("code", FRY), LanguageVariant.class));
        s.setLemmas(new ArrayList<>(1));
        s.getLemmas().add(l);
        l.setSuperlemma(s);
        create(s);
        return LinkHelper.linkFromLemma(l);
    }

    @RolesAllowed({ManagingApi.ROLE})
    @Override
    public LemmaLink createLemma(String lemma, String language, int superlemmaId) {
        Superlemma s = find(superlemmaId, Superlemma.class);
        Lemma l = new Lemma();
        l.setLemma(lemma);
        l.setLanguageVariant(findOne("LanguageVariant.byCode", Param.one("code", language), LanguageVariant.class));
        s.getLemmas().add(l);
        l.setSuperlemma(s);
        update(s);
        return LinkHelper.linkFromLemma(l);
    }

    @RolesAllowed({ManagingApi.ROLE})
    @Override
    public LemmaLink updateLemma(LemmaLink link, String lemma, String linguistics, String meaning) throws IllegalArgumentException, LemmaLinkException {
        Lemma l = find(link.getLemmaId(), Lemma.class);
        if (null==l) {
            throw new IllegalArgumentException("lemma " + link.getLemmaId() + " does not exist in superlemmalist");
        }
        boolean changes = false;
        if (linguistics != null || meaning != null) {
            if (!FRY.equals(l.getLanguageVariant().getCode())) {
                throw new IllegalArgumentException("not allowed to update linguistics or meaning for language " + l.getLanguageVariant().getCode());
            }
        }
        LinkHelper.checkLink(link, l);
        if (!l.getLemma().equals(lemma)) {
            l.setLemma(lemma);
            changes = true;
        }
        if (!l.getSuperlemma().getPos().equals(linguistics)) {
            l.getSuperlemma().setPos(linguistics);
            changes = true;
        }
        if (!l.getSuperlemma().getMeaning().equals(meaning)) {
            l.getSuperlemma().setMeaning(meaning);
            changes = true;
        }
        if (changes) {
            Lemma update = update(l);
            // flush to update versions
            getEntityManager().flush();
            return LinkHelper.linkFromLemma(update);
        }
        return link;
    }
    private static final String FRY = "fry";

    @RolesAllowed({ManagingApi.ROLE})
    @Override
    public boolean deleteLemma(LemmaLink link) throws LemmaLinkException {
        Lemma l = find(link.getLemmaId(), Lemma.class);
        if (l != null) {
            LinkHelper.checkLink(link, l);
            delete(l);
        }
        return true;
    }

    @Override
    public void checkLink(LemmaLink link) throws LemmaLinkException {
        Lemma find = find(link.getLemmaId(), Lemma.class);
        if (find == null) {
            throw new NoLemmaFoundException("no lemma found for " + LinkHelper.serialize(link));
        }
        LinkHelper.checkLink(link, find);
    }

    @Override
    public void checkLink(String link) throws LemmaLinkException {
        LemmaLink ll = LinkHelper.fromString(link);
        Lemma find = find(ll.getLemmaId(), Lemma.class);
        if (find == null) {
            throw new NoLemmaFoundException("no lemma found for " + link);
        }
        LinkHelper.checkLink(ll, find);
    }

    @Override
    public LemmaLink currentLink(int lemmaId) {
        return LinkHelper.linkFromLemma(find(lemmaId, Lemma.class));
    }

    @Override
    public String getLinkText(LemmaLink link) throws LemmaLinkException {
        checkLink(link);
        return LinkHelper.linkTextFromLemma(find(link.getLemmaId(), Lemma.class));
    }

    @RolesAllowed(ManagingApi.ROLE)
    @Override
    public void delete(EntityInterface t) {
        super.delete(t); //To change body of generated methods, choose Tools | Templates.
    }

    @RolesAllowed(ManagingApi.ROLE)
    @Override
    public <T> T update(T t) {
        return super.update(t); //To change body of generated methods, choose Tools | Templates.
    }

    @RolesAllowed(ManagingApi.ROLE)
    @Override
    public <T> T create(T t) {
        return super.create(t); //To change body of generated methods, choose Tools | Templates.
    }
}
