/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fa.superlemmalist.ejb;

import org.fryske_akademy.services.AbstractCrudServiceEnvers;
import org.fryske_akademy.services.Auditing;
import org.fryske_akademy.services.CrudReadService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import jakarta.ejb.Local;
import jakarta.ejb.Stateless;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import org.fryske_akademy.services.AbstractCrudService;

/**
 *
 * @author eduard
 */
@Local({CrudReadService.class, Auditing.class})
@Stateless
@SuperCrudBean
public class ReadonlyCrudServiceBean extends AbstractCrudServiceEnvers {

    private static final Logger LOGGER = LoggerFactory.getLogger(ReadonlyCrudServiceBean.class.getName());

    @PersistenceContext(unitName = "superlemmalist_unit_ro")
    private EntityManager em;

    @Override
    public EntityManager getEntityManager() {
        return em;
    }
}
