[home](README.md)

## The superlemma list API

### querying and linking
A superlemma is actually a container for lemmas in a certain language variant. The superlemma list can be queried, returning information that can be used to query other systems.  
The superlemma list uses links that consist out of the id and version of a lemma and of its superlemma. Systems can administer these links. The superlemma list provides a function to translate a link into a readable combination of lemma, language, linguistics and meaning.  
The versions in the superlemma list are incremented on changes. This makes it possible to detect changes in a link, either or not automatically.

#### changes

When the superlemma list detects changes an exception (fault) occurs containing the up to date link). Changes are detected when the superlemma list is updated and a link contains different version numbers. You can also directly call checkLink(link);

### homonyms
The superlemma list can be queried by lemma, language variant and/or linguistic characteristics. For homonyms linguistics can be used to find the right superlemma, but this may be insufficient. In that case the meaning in the superlemmas has to be inspected by hand.

### register applications
In the future applications may be registered with the superlemmalist. An application provides a base url, to which {link} is appended for querying and changes/{oldlink}/{newlink} is appended to notify changes.

### Examples

1. query superlemma list with `lemma=aaigiel,language=fry,linguisticCode=pos.noun` returns a link.
2. querying a lexicon with this link returns the paradigm

Or

1. query superlemma list with `lemma=aaigiel,language=fry,linguisticCode=Pos.adj` returns another link.
2. querying a lexicon with this link returns the paradigm

Or

1. query superlemma list with `lemma=aaigiel,language=fry` returns two links
2. querying a lexicon with these links returns the paradigm for both homonyms

Or

1. query superlemma list with `language=ofs` returns superlemmas that contain old frisian lemma's

An example the other way around:

1. query superlemma list with a link returns `lemma=aaigiel,language=fry,linguisticCode=pos.adj`
2. query corpora with `lemma=aaigiel,language=fry,linguisticCode=pos.adj` return kwic results or document id's

### Query functions

LinguisticCode is suggested to be a part of speech tag (see https://bitbucket.org/fryske-akademy/tei-encoding), or abbr.yes, or prontype.art

The API supports the following functions. In pseudo code:

* `superlemma* findSuperlemmas(lemma?,language*,linguisticCode*)`
* `link* findLinks(lemma?,language*,linguisticCode*)`
* `linkText getLinkText(link)`
* `boolean isLemmaChanged(link)`
* `boolean isSuperlemmaChanged(link)`
* `string* convertFaCode(string)`
* `language* listLanguages()`
* `string* listLinguisticCodes()`
* `string* listFaCodes()`
* `string translate(link, language)`

superlemma information contains:

- id of superlemma
- code of superlemma
- linguisticCode
- meaning
- lemma of lemmas
- language variant of lemmas
- link for lemmas

linkText information contains:

- lemma of lemma
- language variant of lemma
- linguistics
- meaning

for a superlemma that is a construct, the code is the lemma plus the language "lemma"-"language".

### CRUD functions

A small crud API enables managing information from a linking system that is already managed anyway, to minimize extra work. In the superlemmalist, one language variant is the basis for a superlemma. In our case this is modern Frisian. If there is no lemma in the base language a construct is used, we suggest to use <lemma>-<languagecode>.

#### error conditions

- It is an error when a superlemma is updated using a language other than the base language.
- It is an error when a superlemma is updated and the provided link is out of date.

#### functions

- link createLemma (lemma, language, superlemmaId)
- link createSuperlemma (lemma, linguistics?, meaning?)
- link updateLemma (link,lemma, linguistics?, meaning?)
- boolean deleteLemma(link)
