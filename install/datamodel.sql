--
-- PostgreSQL database dump
--

-- Dumped from database version 12.9 (Ubuntu 12.9-0ubuntu0.20.04.1)
-- Dumped by pg_dump version 12.9 (Ubuntu 12.9-0ubuntu0.20.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: hibernate_sequence; Type: SEQUENCE; Schema: public; Owner: super
--

CREATE SEQUENCE public.hibernate_sequence
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.hibernate_sequence OWNER TO super;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: language_variant; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.language_variant (
    id integer NOT NULL,
    version integer NOT NULL,
    code character varying(30) NOT NULL,
    description character varying(255) DEFAULT NULL::character varying
);


ALTER TABLE public.language_variant OWNER TO super;

--
-- Name: language_variant_aud; Type: TABLE; Schema: public; Owner: super
--

CREATE TABLE public.language_variant_aud (
    id integer NOT NULL,
    rev integer NOT NULL,
    revtype smallint,
    code character varying(30),
    description character varying(255)
);


ALTER TABLE public.language_variant_aud OWNER TO super;

--
-- Name: language_variant_id_seq; Type: SEQUENCE; Schema: public; Owner: super
--

CREATE SEQUENCE public.language_variant_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.language_variant_id_seq OWNER TO super;

--
-- Name: language_variant_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: super
--

ALTER SEQUENCE public.language_variant_id_seq OWNED BY public.language_variant.id;


--
-- Name: lemma; Type: TABLE; Schema: public; Owner: super
--

CREATE TABLE public.lemma (
    id integer NOT NULL,
    version integer NOT NULL,
    lemma character varying(50),
    language_variant_id integer,
    superlemma_id integer
);


ALTER TABLE public.lemma OWNER TO super;

--
-- Name: lemma_aud; Type: TABLE; Schema: public; Owner: super
--

CREATE TABLE public.lemma_aud (
    id integer NOT NULL,
    rev integer NOT NULL,
    revtype smallint,
    lemma character varying(50),
    language_variant_id integer,
    superlemma_id integer
);


ALTER TABLE public.lemma_aud OWNER TO super;

--
-- Name: lemma_id_seq; Type: SEQUENCE; Schema: public; Owner: super
--

CREATE SEQUENCE public.lemma_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.lemma_id_seq OWNER TO super;

--
-- Name: lemma_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: super
--

ALTER SEQUENCE public.lemma_id_seq OWNED BY public.lemma.id;


--
-- Name: revisioninfo; Type: TABLE; Schema: public; Owner: super
--

CREATE TABLE public.revisioninfo (
    id integer NOT NULL,
    "timestamp" bigint NOT NULL,
    username character varying(20)
);


ALTER TABLE public.revisioninfo OWNER TO super;

--
-- Name: superlemma; Type: TABLE; Schema: public; Owner: super
--

CREATE TABLE public.superlemma (
    id integer NOT NULL,
    version integer NOT NULL,
    code character varying(50),
    pos character varying(20),
    meaning character varying(255)
);


ALTER TABLE public.superlemma OWNER TO super;

--
-- Name: superlemma_aud; Type: TABLE; Schema: public; Owner: super
--

CREATE TABLE public.superlemma_aud (
    id integer NOT NULL,
    rev integer NOT NULL,
    revtype smallint,
    code character varying(50),
    pos character varying(20),
    meaning character varying(255)
);


ALTER TABLE public.superlemma_aud OWNER TO super;

--
-- Name: superlemma_id_seq; Type: SEQUENCE; Schema: public; Owner: super
--

CREATE SEQUENCE public.superlemma_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.superlemma_id_seq OWNER TO super;

--
-- Name: superlemma_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: super
--

ALTER SEQUENCE public.superlemma_id_seq OWNED BY public.superlemma.id;


--
-- Name: language_variant id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.language_variant ALTER COLUMN id SET DEFAULT nextval('public.language_variant_id_seq'::regclass);


--
-- Name: lemma id; Type: DEFAULT; Schema: public; Owner: super
--

ALTER TABLE ONLY public.lemma ALTER COLUMN id SET DEFAULT nextval('public.lemma_id_seq'::regclass);


--
-- Name: superlemma id; Type: DEFAULT; Schema: public; Owner: super
--

ALTER TABLE ONLY public.superlemma ALTER COLUMN id SET DEFAULT nextval('public.superlemma_id_seq'::regclass);


--
-- Name: hibernate_sequence; Type: SEQUENCE SET; Schema: public; Owner: super
--

SELECT pg_catalog.setval('public.hibernate_sequence', 1, false);


--
-- Name: language_variant_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.language_variant_id_seq', 1, false);


--
-- Name: lemma_id_seq; Type: SEQUENCE SET; Schema: public; Owner: super
--

SELECT pg_catalog.setval('public.lemma_id_seq', 1, false);


--
-- Name: superlemma_id_seq; Type: SEQUENCE SET; Schema: public; Owner: super
--

SELECT pg_catalog.setval('public.superlemma_id_seq', 1, false);


--
-- Name: language_variant_aud language_variant_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: super
--

ALTER TABLE ONLY public.language_variant_aud
    ADD CONSTRAINT language_variant_aud_pkey PRIMARY KEY (id, rev);


--
-- Name: language_variant language_variant_code_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.language_variant
    ADD CONSTRAINT language_variant_code_key UNIQUE (code);


--
-- Name: language_variant language_variant_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.language_variant
    ADD CONSTRAINT language_variant_pkey PRIMARY KEY (id);


--
-- Name: lemma_aud lemma_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: super
--

ALTER TABLE ONLY public.lemma_aud
    ADD CONSTRAINT lemma_aud_pkey PRIMARY KEY (id, rev);


--
-- Name: lemma lemma_pkey; Type: CONSTRAINT; Schema: public; Owner: super
--

ALTER TABLE ONLY public.lemma
    ADD CONSTRAINT lemma_pkey PRIMARY KEY (id);


--
-- Name: revisioninfo revisioninfo_pkey; Type: CONSTRAINT; Schema: public; Owner: super
--

ALTER TABLE ONLY public.revisioninfo
    ADD CONSTRAINT revisioninfo_pkey PRIMARY KEY (id);


--
-- Name: superlemma_aud superlemma_aud_pkey; Type: CONSTRAINT; Schema: public; Owner: super
--

ALTER TABLE ONLY public.superlemma_aud
    ADD CONSTRAINT superlemma_aud_pkey PRIMARY KEY (id, rev);


--
-- Name: superlemma superlemma_pkey; Type: CONSTRAINT; Schema: public; Owner: super
--

ALTER TABLE ONLY public.superlemma
    ADD CONSTRAINT superlemma_pkey PRIMARY KEY (id);


--
-- Name: lemma fk82n70x8dpu8sdcdrjs0394x99; Type: FK CONSTRAINT; Schema: public; Owner: super
--

ALTER TABLE ONLY public.lemma
    ADD CONSTRAINT fk82n70x8dpu8sdcdrjs0394x99 FOREIGN KEY (superlemma_id) REFERENCES public.superlemma(id);


--
-- Name: lemma_aud fkly562x123q1c6wol27tagrl9d; Type: FK CONSTRAINT; Schema: public; Owner: super
--

ALTER TABLE ONLY public.lemma_aud
    ADD CONSTRAINT fkly562x123q1c6wol27tagrl9d FOREIGN KEY (rev) REFERENCES public.revisioninfo(id);


--
-- Name: language_variant_aud fkqj27bwudrohwrjurb4dpmiair; Type: FK CONSTRAINT; Schema: public; Owner: super
--

ALTER TABLE ONLY public.language_variant_aud
    ADD CONSTRAINT fkqj27bwudrohwrjurb4dpmiair FOREIGN KEY (rev) REFERENCES public.revisioninfo(id);


--
-- Name: superlemma_aud fkrktigkyy54yyq2doey4tt05qt; Type: FK CONSTRAINT; Schema: public; Owner: super
--

ALTER TABLE ONLY public.superlemma_aud
    ADD CONSTRAINT fkrktigkyy54yyq2doey4tt05qt FOREIGN KEY (rev) REFERENCES public.revisioninfo(id);

create unique index lemma_unique on lemma (lemma, language_variant_id, superlemma_id);

create unique index superlemma_unique on superlemma (code, pos, meaning);

create index code_index on superlemma (code);

create index lemma_index on lemma (lemma);

-- alter sequence lemma_id_seq restart with 176020;