-- \c superlemmalist;
-- 
-- delete from lemma;
-- delete from superlemma;
-- delete from language_variant;
insert into language_variant (version,description,code) values (0,'Old Frisian','ofs');
insert into language_variant (version,description,code) values (0,'Western Frisian','fry');
insert into language_variant (version,description,code) values (0,'Frisian dialects before 1800','fry-x-pre1800');
insert into language_variant (version,description,code) values (0,'Dutch','nld');
insert into language_variant (version,description,code) values (0,'Dialects','fry-x-dialects');
insert into language_variant (version,description,code) values (0,'Harlingerlânsk','fry-x-harl');
insert into language_variant (version,description,code) values (0,'Runes','ofs-x-runes');
insert into language_variant (version,description,code) values (0,'Mid Frisian','fry-x-midfrisian');
insert into language_variant (version,description,code) values (0,'Bildts','fry-x-bildts');

insert into superlemma (id,version,code, meaning, pos)
select l.id,0,l.foarm,
case 
    when not l.betsj is null then l.betsj
    when not l.opmerking is null then l.opmerking
    else ''
end,
case
    when position(' ' in c.universalcode) = 0 then c.universalcode
    else substring(c.universalcode from 1 for position(' ' in c.universalcode) - 1)
end
from standertwurdlist.Lemma l, code_convert c
where c.facode=l.soart
and l.neisjoen = true and l.stdlem_id is null;

insert into lemma (id,version,lemma,superlemma_id, language_variant_id)
select s.id, 0, s.code, s.id, lv.id
from superlemma s, language_variant lv
where lv.code='fry';
