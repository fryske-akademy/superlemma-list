#!/bin/bash

echo -n "payara admin password: "
read -s ppw

echo AS_ADMIN_PASSWORD=$ppw > /tmp/.paya
chmod 700 /tmp/.paya

../../payara5/bin/asadmin -u admin -W /tmp/.paya multimode --file asadmin_commands.txt
[ $? -eq 0 ] || exit $?

rm /tmp/.paya
