#!/bin/sh
# maakt een backup voor elke weekdag in een docker volume
pg_dumpall > /reports/backup-`date +%w`.sql