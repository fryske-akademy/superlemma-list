package org.fryske_akademy.graphql.fetchers;

import com.vectorprint.configuration.cdi.Property;
import graphql.schema.DataFetchingEnvironment;
import org.fa.superlemmalist.graphql.LanguageFetcher;
import org.fa.superlemmalist.graphql.SuperlemmasFetcher;
import org.fryske_akademy.services.CrudReadService;
import org.fryske_akademy.superlemmalist.Superlemmas;

import jakarta.enterprise.context.RequestScoped;
import jakarta.inject.Inject;

@RequestScoped
public class LanguageFetcherImpl implements LanguageFetcher {
}
