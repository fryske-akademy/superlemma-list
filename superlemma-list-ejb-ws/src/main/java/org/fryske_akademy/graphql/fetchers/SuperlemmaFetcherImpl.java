package org.fryske_akademy.graphql.fetchers;

import graphql.schema.DataFetchingEnvironment;
import org.fa.superlemmalist.graphql.SuperlemmaFetcher;
import org.fryske_akademy.superlemmalist.Superlemma;

import jakarta.enterprise.context.RequestScoped;
import jakarta.inject.Inject;
import org.fa.superlemmalist.ejb.SuperCrudBean;
import org.fryske_akademy.services.CrudReadService;

@RequestScoped
public class SuperlemmaFetcherImpl implements SuperlemmaFetcher {

    @Inject
    @SuperCrudBean
    private CrudReadService crudReadService;

    @Override
    public Superlemma get(DataFetchingEnvironment environment) throws Exception {
        return Superlemma.builder().build();
    }
}
