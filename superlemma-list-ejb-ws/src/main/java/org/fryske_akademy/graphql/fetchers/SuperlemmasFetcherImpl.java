package org.fryske_akademy.graphql.fetchers;

import com.vectorprint.configuration.cdi.Property;
import graphql.schema.DataFetchingEnvironment;
import org.fa.superlemmalist.graphql.SuperlemmasFetcher;
import org.fa.superlemmalist.jpa.LanguageVariant;
import org.fa.superlemmalist.jpa.Lemma;
import org.fa.superlemmalist.jpa.Superlemma;
import org.fryske_akademy.jpa.Param;
import org.fryske_akademy.superlemmalist.ISO639_2;
import org.fryske_akademy.superlemmalist.Language;
import org.fryske_akademy.superlemmalist.Pos;
import org.fryske_akademy.superlemmalist.Superlemmas;

import jakarta.enterprise.context.RequestScoped;
import jakarta.inject.Inject;
import org.fa.superlemmalist.ejb.SuperCrudBean;
import org.fryske_akademy.services.CrudReadService;

import java.util.List;
import java.util.stream.Collectors;

@RequestScoped
public class SuperlemmasFetcherImpl implements SuperlemmasFetcher {

    @Inject
    @SuperCrudBean
    private CrudReadService crudReadService;

    @Inject
    @Property(defaultValue = "30")
    private int pagingMax;

    public int getPagingMax() {
        return pagingMax;
    }

    @Override
    public Superlemmas get(DataFetchingEnvironment environment) throws Exception {
        int offset = environment.getArgumentOrDefault("offset", 0);
        int max = Integer.min(environment.getArgumentOrDefault("max", 5), getPagingMax());
        String lemma = environment.getArgumentOrDefault("lemma","");
        String p = environment.getArgumentOrDefault("pos","");
        String l = environment.getArgumentOrDefault("language","");
        Pos pos = p.isEmpty() ? null : Pos.valueOf(p);
        Language lang = l.isEmpty() ? null : crudReadService.findDynamic(0,1,null,
                new Param.Builder().add("code",l).build()
                , LanguageVariant.class).get(0).toGraphql();

        Param.Builder builder = new Param.Builder().add("lemma", lemma);
        if (pos!=null) builder.add("superlemma.pos","pos",Superlemma.fromGraphqlPos(pos),false);
        if (lang!=null) builder.add("languageVariant.code","lang",
                lang.getLanguagecode().name().replace('_','-'),false);
        List<Superlemma> superlemmas = crudReadService.streamDynamic(0, -1, null, builder.build(), Lemma.class)
                .map(le -> le.getSuperlemma())
                .distinct()
                .collect(Collectors.toList());
        List<Superlemma> sub = (offset < superlemmas.size() && offset + max + 1 < superlemmas.size()) ?
                superlemmas.subList(offset,offset+max+1) :
                (offset< superlemmas.size()) ? superlemmas.subList(offset,superlemmas.size()) : superlemmas;

        return Superlemmas.builder()
                .setOffset(offset)
                .setMax(max)
                .setTotal(superlemmas.size())
                .setSuperlemmas(sub.stream().map(s->s.toGraphql()).collect(Collectors.toList()))
                .build();
    }
}
