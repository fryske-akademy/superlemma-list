## dealing with changes

Changes in linking systems or the superlemma list may cause these to become 'out of sync'. Here we describe some scenario's and measures. One measure is that the id of a superlemma or a lemma never changes. The concept link (see [API](API.md)) in combination with jpa version mechanism makes detecting changes in the list and in calling systems possibe.

### A change in the superlemma list may effect ‘linking’ systems

when a code changes  
when a superlemma is split  
when a lemma changes  
when a lemma is connected to another superlemma  
...or another language variant  
other less likely changes

* changes in the superlemmalist are logged and may be published to registered systems (future)
* The impact of all changes in the superlemma list has to be determined individually to define appropriate steps

### A change in a ‘linking’ system may require a change (= new version) in the superlemma list

when a lemma changes (the characters of the lemma change, not the choice for a different lemma) the corresponding lemma and possibly superlemma should change as well.  
when a meaning changes, the corresponding meaning in the superlemmalist should be changed  
when linguistics changes, the corresponding linguistics in the superlemmalist should be changed  

linking systems are managed separately, so

* instructions should be provided to indicate if a change may effect the superlemma list
* user friendly check tools should be provided to see the information from the superlemma list after the change
* when a superlemma list change is needed, see the paragraph above

### automatic synchronization

Software may be developed that reports possible 'out of sync' situations. Perhaps some detected situations can be repaired automatically. It remains to be seen how usefull this is.