BEGIN {
    sup=0
    print "<list>"
}

/<superl>[^-+]*$/ {
    print gensub(/superl/,"fry","g")
    sup=1
    next
}
/<foarm>/ && sup==1 {
    print gensub(/<\/foarm/,"</ofs","g",gensub(/<foarm/,"<ofs","g"))
    print ""
    sup=0
}
END {
    print "</list>"
}