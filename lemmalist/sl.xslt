<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:fun="http://fun"
                version="3.1">

    <xsl:output encoding="utf-8" indent="yes" method="text"/>

    <xsl:key name="super" match="lemma" use="superlemma"/>
    <xsl:key name="id" match="lemma" use="id"/>

    <xsl:variable name="tab" select="'&#9;'"/>
    <xsl:variable name="nl" select="'&#10;'"/>

    <xsl:template match="lemmalist">
ofs<xsl:value-of select="$tab"/>fry<xsl:value-of select="$nl"/>
        <xsl:apply-templates/>
    </xsl:template>

<!--    TODO recursief zoeken naar modern friese superlemmas
        alle vormen met 2 streepjes erin overslaan
-->

    <xsl:function name="fun:isFry">
        <xsl:param name="lemma"/>
        <xsl:sequence select="$lemma/id &lt; 1000000000"/>
    </xsl:function>

    <xsl:variable name="validform" select="'^[a-zA-Z]'"/>

    <xsl:template name="findFryLemma">
        <xsl:param name="lid"/>
        <xsl:param name="prev"/>
<!--        <xsl:message terminate="no">-->
<!--            <xsl:value-of select="$prev"/>: <xsl:value-of select="$lid"/>-->
<!--        </xsl:message>-->
        <xsl:if test="not(exists(index-of($prev,$lid)))">
            <xsl:variable name="lemma" select="head(key('id',$lid))"/>
            <xsl:choose>
                <xsl:when test="$lemma[superlemma]"><xsl:value-of select="fun:stripFrm(head(key('id',$lemma/superlemma))/foarm)"/></xsl:when>
                <xsl:when test="$lemma[l1id]"><xsl:for-each select="$lemma/l1id|$lemma/l2id|$lemma/l3id">
                    <xsl:call-template name="findFryLemma"><xsl:with-param name="lid" select="."/><xsl:with-param name="prev" select="($prev,$lid)"/></xsl:call-template>
                </xsl:for-each></xsl:when>
                <xsl:otherwise><xsl:value-of select="''"/></xsl:otherwise>
            </xsl:choose>
        </xsl:if>
    </xsl:template>

    <xsl:function name="fun:stripFrm">
        <xsl:param name="foarm"/>
        <xsl:choose>
            <xsl:when test="contains($foarm,'--')"></xsl:when>
            <xsl:otherwise><xsl:value-of select="$foarm"/></xsl:otherwise>
        </xsl:choose>
    </xsl:function>

    <xsl:template match="lemma[not(fun:isFry(.)) and l1id]">
        <xsl:variable name="fry">
            <xsl:for-each select="(l1id,l2id,l3id)">
                <xsl:call-template name="findFryLemma"><xsl:with-param name="lid" select="."/></xsl:call-template>
            </xsl:for-each>
        </xsl:variable>
        <xsl:if test="matches($fry,$validform)">
            <xsl:value-of select="foarm"/><xsl:value-of select="$tab"/><xsl:value-of select="$fry"/><xsl:value-of select="$nl"/>
        </xsl:if>
    </xsl:template>

    <xsl:template match="lemma[not(fun:isFry(.)) and superlemma]" priority="1">
        <xsl:variable name="sl" select="superlemma"/>
        <xsl:variable name="fry" select="fun:stripFrm(head(key('id',$sl))/foarm)"/>
        <xsl:if test="matches($fry,$validform)">
            <xsl:value-of select="foarm"/><xsl:value-of select="$tab"/><xsl:value-of select="$fry"/><xsl:value-of select="$nl"/>
        </xsl:if>
    </xsl:template>

    <xsl:template match="node()|@*" priority="-1">
        <xsl:apply-templates select="@*|node()"/>
    </xsl:template>

</xsl:stylesheet>