package org.fa.superlemmalist.graphql;

import graphql.schema.DataFetcher;
import org.fryske_akademy.superlemmalist.Superlemmas;

public interface SuperlemmasFetcher extends DataFetcher<Superlemmas> {
    public static final String SUPERLEMMAS = "findsuperlemmas";
}
