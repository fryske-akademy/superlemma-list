package org.fa.superlemmalist.graphql;

import graphql.schema.DataFetcher;
import org.fryske_akademy.superlemmalist.Superlemma;
import org.fryske_akademy.superlemmalist.Superlemmas;

public interface SuperlemmaFetcher extends DataFetcher<Superlemma> {
    public static final String SUPERLEMMA = "getsuperlemma";
}
