package org.fa.superlemmalist.graphql;

import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import org.fryske_akademy.superlemmalist.ISO639_2;
import org.fryske_akademy.superlemmalist.Language;
import org.fryske_akademy.superlemmalist.Pos;

import jakarta.enterprise.context.ApplicationScoped;
import java.util.Arrays;
import java.util.stream.Collectors;

public interface PosFetcher extends DataFetcher<Pos[]> {
    public static final String POS = "pos";

    @Override
    default Pos[] get(DataFetchingEnvironment environment) throws Exception {
        return Pos.values();
    }
}
