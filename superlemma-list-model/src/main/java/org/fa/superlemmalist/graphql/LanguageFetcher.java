package org.fa.superlemmalist.graphql;

import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import org.fryske_akademy.superlemmalist.ISO639_2;
import org.fryske_akademy.superlemmalist.Language;
import org.fryske_akademy.superlemmalist.Superlemmas;

import java.util.Arrays;
import java.util.stream.Collectors;

public interface LanguageFetcher extends DataFetcher<Language[]> {
    public static final String LANGUAGES = "languages";

    @Override
    default Language[] get(DataFetchingEnvironment environment) throws Exception {
        return Arrays.stream(ISO639_2.values()).map(i ->
            Language.builder().setLanguagecode(i).build()
        ).collect(Collectors.toList()).toArray(new Language[ISO639_2.values().length]);
    }
}
