/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fa.superlemmalist.jpa.listeners;

import jakarta.persistence.PersistenceException;
import jakarta.persistence.PrePersist;
import jakarta.persistence.PreUpdate;
import org.fa.superlemmalist.jpa.Lemma;
import org.fa.tei.jaxb.v2_x.Join;

import java.util.Locale;

/**
 *
 * @author eduard
 */
public class LemmaListener  {

    @PreUpdate
    @PrePersist
    public void check(Lemma toStore) {
        String l = toStore.getSuperlemma().getPos();
        try {
            Join.Pos.valueOf(l.toLowerCase(Locale.ROOT));
        } catch (IllegalArgumentException e) {
            throw new PersistenceException(e);
        }
    }

}
