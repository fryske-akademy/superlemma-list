/*
 * Copyright 2018 Fryske Akademy.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.fa.superlemmalist.jpa;

import jakarta.persistence.Access;
import jakarta.persistence.AccessType;
import jakarta.persistence.Entity;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import org.fa.superlemmalist.jpa.listeners.LemmaListener;
import org.fa.superlemmalist.linking.LinkHelper;
import org.fryske_akademy.jpa.AbstractEntity;
import org.hibernate.envers.Audited;

/**
 *
 * @author eduard
 */
@Entity
@Audited
@Table(name="lemma")
@Access(AccessType.FIELD)
@NamedQueries({
    @NamedQuery(name = "Lemma.likeLemma",query = "select l from Lemma l where l.lemma like :lemma"),
    @NamedQuery(name = "Lemma.byLemma",query = "select l from Lemma l where l.lemma=:lemma"),
    @NamedQuery(name = "Lemma.byLemmaAndLanguage",query = "select l from Lemma l where l.lemma=:lemma and l.languageVariant.code=:language"),
    @NamedQuery(name = "Lemma.byLemmaAndLanguageAndId",query = "select l from Lemma l where "
            + "l.lemma=:lemma and l.languageVariant.code=:language and l.superlemma.id=:id")
})
@EntityListeners(LemmaListener.class)
public class Lemma extends AbstractEntity {
    
    private String lemma;
    @ManyToOne
    @JoinColumn(name = "superlemma_id", referencedColumnName = "id")
    private Superlemma superlemma;
    @ManyToOne
    @JoinColumn(name = "language_variant_id", referencedColumnName = "id")
    private LanguageVariant languageVariant;

    public String getLemma() {
        return lemma;
    }

    public void setLemma(String lemma) {
        this.lemma = lemma;
    }

    public Superlemma getSuperlemma() {
        return superlemma;
    }

    public void setSuperlemma(Superlemma superlemma) {
        this.superlemma = superlemma;
    }

    public LanguageVariant getLanguageVariant() {
        return languageVariant;
    }

    public void setLanguageVariant(LanguageVariant languageVariant) {
        this.languageVariant = languageVariant;
    }
    
    
    @Transient
    public LemmaLink getLink() {
        return LinkHelper.linkFromLemma(this);
    }
    
}
