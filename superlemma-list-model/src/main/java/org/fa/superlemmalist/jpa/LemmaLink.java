/*
 * Copyright 2022 Fryske Akademy.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.fa.superlemmalist.jpa;

import java.io.Serializable;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.IdClass;

/**
 *
 * @author eduard
 */
public class LemmaLink implements Serializable {
    private Integer superlemmaId;
    private Integer superlemmaVersion;
    private Integer lemmaId;
    private Integer lemmaVersion;

    public Integer getSuperlemmaId() {
        return superlemmaId;
    }

    public void setSuperlemmaId(Integer superlemmaId) {
        this.superlemmaId = superlemmaId;
    }

    public Integer getSuperlemmaVersion() {
        return superlemmaVersion;
    }

    public void setSuperlemmaVersion(Integer superlemmaVersion) {
        this.superlemmaVersion = superlemmaVersion;
    }

    public Integer getLemmaId() {
        return lemmaId;
    }

    public void setLemmaId(Integer lemmaId) {
        this.lemmaId = lemmaId;
    }

    public Integer getLemmaVersion() {
        return lemmaVersion;
    }

    public void setLemmaVersion(Integer lemmaVersion) {
        this.lemmaVersion = lemmaVersion;
    }
    
    public org.fryske_akademy.superlemmalist.LemmaLink toGraphql() {
        return org.fryske_akademy.superlemmalist.LemmaLink.builder()
                .setLemmaid(getLemmaId())
                .setLemmaversion(getLemmaVersion())
                .setSuperlemmaid(getSuperlemmaId())
                .setSuperlemmaversion(getSuperlemmaVersion())
                .build();
    }
    
}
