/*
 * Copyright 2018 Fryske Akademy.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.fa.superlemmalist.jpa;

import org.fa.superlemmalist.jpa.listeners.SuperlemmaListener;
import org.fryske_akademy.jpa.AbstractEntity;
import org.fryske_akademy.superlemmalist.Pos;
import org.hibernate.envers.Audited;

import jakarta.persistence.Access;
import jakarta.persistence.AccessType;
import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EntityListeners;
import jakarta.persistence.FetchType;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

/**
 *
 * @author eduard
 */
@Entity
@Audited
@Table(name="superlemma")
@Access(AccessType.FIELD)
@NamedQueries({
        @NamedQuery(name = "Superlemma.byCode",query = "select s from Superlemma s where s.code=:code"),
        @NamedQuery(name = "Superlemma.byCodeAndPos",query = "select s from Superlemma s where s.code=:code and s.pos=:pos"),
        @NamedQuery(name = "Superlemma.byCodeAndPosAndMeaning",query = "select s from Superlemma s where s.code=:code and s.pos=:pos"
                + " and s.meaning=:meaning"),
        @NamedQuery(name = "Superlemma.likeCode",query = "select s from Superlemma s where s.code like :code")
})
@EntityListeners(SuperlemmaListener.class)
public class Superlemma extends AbstractEntity {

    @Column
    private String code;
    @OneToMany(mappedBy = "superlemma", cascade = {CascadeType.PERSIST, CascadeType.REMOVE}, fetch = FetchType.EAGER)
    private List<Lemma> lemmas;
    @Column
    private String pos;
    @Column
    private String meaning;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public List<Lemma> getLemmas() {
        return lemmas;
    }

    public void setLemmas(List<Lemma> lemmas) {
        this.lemmas = lemmas;
    }

    public String getPos() {
        return pos;
    }

    public void setPos(String pos) {
        this.pos = pos;
    }

    @Transient
    public Pos graphqlPos() {
        return Pos.valueOf(pos.replace('.','_').toLowerCase(Locale.ROOT));
    }

    @Transient
    public static String fromGraphqlPos(Pos pos) {
        return "Pos."+pos.name().substring(pos.name().indexOf("_")+1).toUpperCase(Locale.ROOT);
    }

    public String getMeaning() {
        return meaning;
    }

    public void setMeaning(String meaning) {
        this.meaning = meaning;
    }
    
    @Transient
    public org.fryske_akademy.superlemmalist.Superlemma toGraphql() {
        return org.fryske_akademy.superlemmalist.Superlemma.builder()
                .setCode(getCode())
                .setMeaning(getMeaning())
                .setPos(graphqlPos())
                .setLemmas(lemmas.stream().map(l ->
                                org.fryske_akademy.superlemmalist.Lemma.builder()
                                .setLemma(l.getLemma())
                                .setLanguagecode(l.getLanguageVariant().graphqlCode())
                                .setLink(l.getLink().toGraphql())
                                .build()
                        ).collect(Collectors.toList()))
                .build();
    }
    
    
}
