/*
 * Copyright 2018 Fryske Akademy.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.fa.superlemmalist.jpa;

import jakarta.persistence.Access;
import jakarta.persistence.AccessType;
import jakarta.persistence.Cacheable;
import jakarta.persistence.Entity;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;

import org.fryske_akademy.jpa.AbstractEntity;
import org.fryske_akademy.superlemmalist.Language;
import org.hibernate.envers.Audited;

/**
 *
 * @author eduard
 */
@Entity
@Audited
@Table(name="language_variant")
@Cacheable
@Access(AccessType.FIELD)
@NamedQueries({
    @NamedQuery(name = "LanguageVariant.byCode",query = "select l from LanguageVariant l where l.code=:code"),
    @NamedQuery(name = "LanguageVariant.likeCode",query = "select l from LanguageVariant l where l.code like :code")
})
public class LanguageVariant extends AbstractEntity {
        
    private String code;
    private String description;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Transient
    public org.fryske_akademy.superlemmalist.ISO639_2 graphqlCode() {
        return org.fryske_akademy.superlemmalist.ISO639_2.valueOf(code.replace('-','_'));
    }

    @Transient
    public Language toGraphql() {
        return Language.builder()
                .setLanguagecode(graphqlCode())
                .setDescription(description)
                .build();
    }
    
}
