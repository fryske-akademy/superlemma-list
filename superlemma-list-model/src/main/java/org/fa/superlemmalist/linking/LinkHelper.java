/*
 * Copyright 2018 Fryske Akademy.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.fa.superlemmalist.linking;

import org.fa.superlemmalist.jpa.Lemma;
import org.fa.superlemmalist.jpa.LemmaLink;

import java.util.regex.Pattern;

/**
 *
 * @author eduard
 */
public class LinkHelper {

    private LinkHelper() {
    }


    public static LemmaLink linkFromLemma(Lemma lemma) {
        LemmaLink l = new LemmaLink();
        l.setLemmaId(lemma.getId());
        l.setLemmaVersion(lemma.getVersion());
        l.setSuperlemmaId(lemma.getSuperlemma().getId());
        l.setSuperlemmaVersion(lemma.getSuperlemma().getVersion());
        return l;
    }

    public static String fromLemma(Lemma lemma) {
        return (lemma.getSuperlemma()!=null) ? lemma.getSuperlemma().getId() + ","
                + lemma.getSuperlemma().getVersion() + "," + lemma.getId() + ","
                + lemma.getVersion() : null + ","
                + null + "," + lemma.getId() + ","
                + lemma.getVersion();
    }

    public static String linkTextFromLemma(Lemma lemma) {
        String s = lemma.getLemma() + "|" + lemma.getLanguageVariant().getCode();
        return s + "|" + lemma.getSuperlemma().getPos() + "|" + lemma.getSuperlemma().getMeaning();
    }

    public static void checkLink(LemmaLink link, Lemma lemma) throws LemmaLinkException {
        check(link, lemma == null, lemma.getId(), lemma.getVersion(), fromLemma(lemma), linkTextFromLemma(lemma), lemma.getSuperlemma().getId(), lemma.getSuperlemma().getVersion(), lemma);
    }

    private static void check(LemmaLink link, boolean b, Integer id, int version, String s, String s2, Integer id2, int version2, Lemma lemma) throws LemmaChangedException, WrongSuperlemmaException, SuperlemmaChangedException {
        if (link == null || b) {
            throw new IllegalArgumentException("both arguments need to be provided");
        }
        if (link.getLemmaId() != id) {
            throw new IllegalArgumentException("link and lemma have different id's, cannot be checked");
        }
        if (link.getLemmaVersion() != version) {
            throw new LemmaChangedException("up to date link: " + s + " (" + s2 + ")");
        }
        if (link.getSuperlemmaId() != id2) {
            throw new WrongSuperlemmaException("up to date link: " + s + " (" + s2 + ")");
        }
        if (link.getSuperlemmaVersion() != version2) {
            throw new SuperlemmaChangedException("up to date link: " + s + " (" + s2 + ")");
        }
    }

    /**
     * return a string in the form
     * "superlemmaId,superlemmaVersion,lemmaId,lemmaVersion";
     *
     * @param link
     * @return
     */
    public static String serialize(LemmaLink link) {
        return link.getSuperlemmaId() + "," + link.getSuperlemmaVersion() + "," + link.getLemmaId() + "," + link.getLemmaVersion();
    }
    public static final Pattern LEMMALINK_PATTERN = Pattern.compile("^[0-9]+,[0-9]+,[0-9]+,[0-9]+$");

    /**
     * creat a link from a string in the form
     * "superlemmaId,superlemmaVersion,lemmaId,lemmaVersion";
     *
     * @param s
     * @return
     */
    public static LemmaLink fromString(String s) throws FormatException {
        if (!LEMMALINK_PATTERN.matcher(s).matches()) {
            throw new FormatException("wrong format, use " + LEMMALINK_PATTERN.pattern());
        }
        LemmaLink l = new LemmaLink();
        String[] split = s.split(",");
        l.setSuperlemmaId(Integer.parseInt(split[0]));
        l.setSuperlemmaVersion(Integer.parseInt(split[1]));
        l.setLemmaId(Integer.parseInt(split[2]));
        l.setLemmaVersion(Integer.parseInt(split[3]));
        return l;
    }

}
