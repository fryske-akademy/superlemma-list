/*
 * Copyright 2018 Fryske Akademy.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.fa.superlemmalist.ejb;

import org.fa.superlemmalist.jpa.LemmaLink;
import org.fa.superlemmalist.linking.LemmaLinkException;
import org.fa.superlemmalist.linking.LinkHelper;

/**
 * to be used in applications from which data in the superlemma list is managed
 * @see LemmaLink
 * @see LinkHelper#fromString(java.lang.String) 
 * @author eduard
 */
public interface ManagingApi extends SuperlemmaService {
    
    public static final String ROLE = "superlemma_editor";
    
    /**
     * create a superlemma and a lemma under it in the superlemma language (modern Frisian), return the id of the lemma.
     * @param lemma
     * @param linguistics one of the enums (enum classname.constant) in {@link LinguisticValue}
     * @param meaning
     * @return 
     */
    LemmaLink createSuperlemma(String lemma, String linguistics, String meaning);
    /**
     * create a lemma under a superlemma
     * @param lemma
     * @param language
     * @param superlemmaId
     * @return 
     */
    LemmaLink createLemma(String lemma, String language, int superlemmaId);
    /**
     * update a lemma, return a fresh link. When the lemma is in the {@link SuperlemmaType#LANGUAGE superlemma
     * language}, update the superlemma as well.
     * @param link
     * @param lemma
     * @param linguistics one of the enums (enum classname.constant) in {@link LinguisticValue}
     * @param meaning the meaning of the superlemma
     * @return
     * @throws org.fa.superlemmalist.linking.LemmaLinkException
     * @throws IllegalArgumentException when linguistics and/or meaning are provided and the lemma is in a language other then
     * {@link SuperlemmaType#LANGUAGE}
     */
    LemmaLink updateLemma(LemmaLink link, String lemma, String linguistics, String meaning) throws IllegalArgumentException, LemmaLinkException;
    boolean deleteLemma(LemmaLink link) throws LemmaLinkException;
    
    /**
     * 
     * @param lemmaId the id of a lemma in the superlemma list
     * @return 
     */
    LemmaLink currentLink(int lemmaId);

    
}
