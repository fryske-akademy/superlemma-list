package org.fa.superlemmalist.graphql;

/*-
 * #%L
 * languageservlet
 * %%
 * Copyright (C) 2021 Fryske Akademy
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import graphql.kickstart.servlet.GraphQLConfiguration;
import graphql.kickstart.servlet.GraphQLHttpServlet;
import graphql.schema.idl.RuntimeWiring;
import graphql.schema.idl.SchemaGenerator;

import jakarta.annotation.PostConstruct;
import jakarta.inject.Inject;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;

@WebServlet(name = "GraphQLLexiconServlet", urlPatterns = {"/graphql/*"}, loadOnStartup = 1)
public class GraphQLServlet extends GraphQLHttpServlet {

    @Inject
    private SuperlemmasFetcher superlemmasFetcher;
    @Inject
    private SuperlemmaFetcher superlemmaFetcher;
    @Inject
    private LanguageFetcher languageFetcher;
    @Inject
    private PosFetcher posFetcher;

    @Inject
    private GraphQLSchemaBuilder graphQLSchemaBuilder;

    private static GraphQLConfiguration configuration = null;

    @PostConstruct
    private void initConfig() {
        if (configuration == null) {
            createSchema();
            configuration = GraphQLConfiguration
                    .with(graphQLSchemaBuilder.getGraphQLSchema()).build();
        }
    }

    @Override
    protected GraphQLConfiguration getConfiguration() {
        return configuration;
    }


    private void createSchema() {
        RuntimeWiring runtimeWiring = RuntimeWiring.newRuntimeWiring()
                .type("Query", builder -> builder
                        .dataFetcher(SuperlemmasFetcher.SUPERLEMMAS, superlemmasFetcher)
                        .dataFetcher(SuperlemmaFetcher.SUPERLEMMA, superlemmaFetcher)
                        .dataFetcher(PosFetcher.POS, posFetcher)
                        .dataFetcher(LanguageFetcher.LANGUAGES, languageFetcher)
                )
                .wiringFactory(GraphqlSimpleWiring.GRAPHQL_SIMPLE_WIRING)
                .build();
        graphQLSchemaBuilder.setGraphQLSchema(
                new SchemaGenerator().makeExecutableSchema(graphQLSchemaBuilder.getTypeDefinitionRegistry(), runtimeWiring)
        );
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) {
        try {
            req.setCharacterEncoding(StandardCharsets.UTF_8.name());
            resp.setCharacterEncoding(StandardCharsets.UTF_8.name());
        } catch (UnsupportedEncodingException e) {
            throw new IllegalStateException(e);
        }
        super.doPost(req, resp);
    }
}
