<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Superlemma Service</title>
    </head>
    <body>
        <h1>Home of Fryske Akademy superlemma service!</h1>
        
        <p>
            See <a href="graphql/schema.json">graphql/schema.json</a> for the graphql service that is available.
        </p>
        
        <p>Contact: e drenth at fryske-akademy dot nl</p>
    </body>
</html>
