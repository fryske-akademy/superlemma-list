const lastPage = function (tb) {
    let p = PF(tb).getPaginator();
    p.setPage(p.cfg.pageCount - 1);
};
const nextPage = function() {
    PF('datalist').getPaginator().next();
};
const prevPage = function() {
    PF('datalist').getPaginator().prev();
};
const rowdown = function() {
    let t = PF('datalist');
    let index = getSelectedRowIndex()+1;
    //if new index equals number of rows, set index to first row
    if (index >= t.tbody.find('tr').length) {
        index = 0;
    }
    rowSelect(t,index);
};
const rowup = function() {
    let t = PF('datalist');
    let index = getSelectedRowIndex()-1;

    if (index < 0) {
        index = t.tbody.find('tr').length - 1;
    }

    rowSelect(t,index);
};
const rowSelect = function(t,i) {
    t.unselectAllRows();
    t.selectRow(t.findRow(i));
};
const getSelectedRowIndex = function() {
    let t = PF('datalist');
    if (t.selection.length == 1) {
        let row = t.tbody.children('tr[data-rk="' + t.selection[0] + '"]')[0];
        return row ? Number(row.rowIndex - 1) : -1;
    }
    return -1;
};
const saveAll = function() {
    let t = PF('datalist');
    $("tr[style]").each(function(index,row) {
        t.saveRowEdit(t.findRow(row.rowIndex-1));
    });
    PF('datalist').filter();
};
const toggleDialog = function(d) {
    let dia = PF(d);
    if (dia.isVisible()) {
        dia.hide();
    } else {
        dia.show();
    }
};
const changed = function(field) {
    field.parents('td').first().css('border','2px solid rgb(248,156,32)');
    field.parents('tr').first().css('background-color','rgb(248,156,32,0.5)');
};
const scrollDown = function (selector) {
    let c = document.querySelector(selector);
    $(c).animate({scrollTop: c.scrollHeight}, 500);
};
const scrollUp = function (selector) {
    let c = document.querySelector(selector);
    window.setTimeout(function () {
        $(c).animate({scrollTop: 0}, 500);
    }, 600);
};
