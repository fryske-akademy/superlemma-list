/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fa.superlemmalist.jsf.lazy;

import java.util.List;
import jakarta.enterprise.context.SessionScoped;
import jakarta.inject.Named;
import org.fa.superlemmalist.jpa.Lemma;
import org.fa.superlemmalist.jpa.Superlemma;
import org.fa.superlemmalist.jsf.converters.SuperlemmaConverter;
import org.fryske_akademy.jpa.OPERATOR;
import org.fryske_akademy.jpa.Param;

/**
 *
 * @author eduard
 */
@SessionScoped
@Named
public class LazyLemma extends LazySuper<Lemma> {

    @Override
    protected void addToParamBuilder(Param.Builder builder, String key, Object value) {
        if ("superlemma".equals(key)) {
            if (String.valueOf(value).contains(":")) {
                Superlemma find = SuperlemmaConverter.find(String.valueOf(value), getCrudReadService());
                if (find!=null) {
                    builder.add(key, find);
                }
            } else {
                List<Superlemma> find = getCrudReadService().find("Superlemma.likeCode", Param.one("code", value),0,30, Superlemma.class);
                builder.add(key, key,"in",find,false,isUseOr());
            }
        } else if ("languageVariant".equals(key)) {
            builder.add(key+".code", key,OPERATOR.EQ,value,false,isUseOr());
        } else {
            super.addToParamBuilder(builder, key, value);
        }
    }

}
