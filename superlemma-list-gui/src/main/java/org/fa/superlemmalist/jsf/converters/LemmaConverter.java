/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fa.superlemmalist.jsf.converters;

import java.io.Serializable;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.faces.component.UIComponent;
import jakarta.faces.context.FacesContext;
import jakarta.faces.convert.Converter;
import jakarta.faces.convert.ConverterException;
import jakarta.faces.convert.FacesConverter;
import jakarta.inject.Named;
import org.fa.superlemmalist.jpa.Lemma;
import org.fryske_akademy.Util;
import org.fryske_akademy.services.CrudReadService;
import org.fryske_akademy.jpa.Param;

/**
 *
 * @author eduard
 */
@FacesConverter(forClass = Lemma.class, managed = true)
@Named
@ApplicationScoped
public class LemmaConverter extends AbstractConverter implements Converter<Lemma>, Serializable {

    @Override
    public Lemma getAsObject(FacesContext facesContext, UIComponent component, String value) {
        if (value == null || value.length() == 0) {
            return null;
        }
        return find(value, crudReadService);
    }

    public static Lemma find(String k, CrudReadService service) {
        Lemma rv = null;
        if (k.contains(":")) {
            String lemma = Util.split(k, 0);
            String language = Util.split(k, 1);
            String id = Util.split(k, 2);
            if (id == null) {
                rv = service.findOne("Lemma.byLemmaAndLanguage", new Param.Builder()
                        .add("lemma", lemma).add("language", language).build(), Lemma.class);
            } else {
                rv = service.findOne("Lemma.byLemmaAndLanguageAndId", new Param.Builder()
                        .add("lemma", lemma).add("language", language).add("id", id).build(), Lemma.class);
            }
        } else {
            rv = service.findOne("Lemma.byLemma", Param.one("lemma", k), Lemma.class);
        }
        if (rv == null) {
            throw new ConverterException(k + " not a Lemma");
        } else {
            return rv;
        }
    }

    public static String getKey(Lemma l) {
        String k = l.getLemma();
        if (l.getLanguageVariant() != null) {
            k += ": " + l.getLanguageVariant().getCode();
        }
        if (l.getSuperlemma() != null) {
            k += l.getSuperlemma().getId();
        }
        return k;
    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent component, Lemma o) {
        if (o == null) {
            return null;
        }
        return getKey( o);
    }
}
