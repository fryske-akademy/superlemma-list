/*
 * Copyright 2018 Fryske Akademy.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.fa.superlemmalist.jsf.lazy;

import org.fa.superlemmalist.ejb.SuperCrudBean;
import org.fryske_akademy.Util;
import org.fryske_akademy.jpa.EntityInterface;
import org.fryske_akademy.jpa.Param;
import org.fryske_akademy.jsf.lazy.NewSupportingLazyModel;
import org.fryske_akademy.services.Auditing;
import org.fryske_akademy.services.CrudReadService;

import jakarta.inject.Inject;
import java.io.IOException;
import java.io.ObjectInputStream;

import static org.fa.superlemmalist.jsf.SuperController.SUPER_ANNOTATION;

/**
 *
 * @author eduard
 */
public abstract class LazySuper<E extends EntityInterface> extends NewSupportingLazyModel<E> {
    
    @Inject
    @SuperCrudBean
    private transient CrudReadService crudReadService;
    
    private void readObject(ObjectInputStream inputStream) throws IOException, ClassNotFoundException {
        inputStream.defaultReadObject();
        crudReadService = Util.getBean(CrudReadService.class, SUPER_ANNOTATION);
    }

    @Override
    public CrudReadService getCrudReadService() {
        return crudReadService;
    }

    @Override
    protected void addToParamBuilder(Param.Builder builder, String key, Object value) {
        builder.add(key,value,isUseOr());
    }
}
