package org.fa.superlemmalist.jsf;

import org.fa.superlemmalist.ejb.SuperCrudBean;
import org.fryske_akademy.Util;
import org.fryske_akademy.jpa.AbstractEntity;
import org.fryske_akademy.jsf.NewSupportingLazyController;
import org.fryske_akademy.jsf.lazy.NewSupportingLazyModel;
import org.fryske_akademy.services.CrudReadService;
import org.fryske_akademy.services.CrudWriteService;

import jakarta.enterprise.util.AnnotationLiteral;
import jakarta.faces.context.FacesContext;
import jakarta.inject.Inject;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.HashSet;
import java.util.Set;

public abstract class SuperController<T extends AbstractEntity, R extends NewSupportingLazyModel<T>> extends NewSupportingLazyController<T, R> {


    @Inject
    @SuperCrudBean
    private transient CrudReadService crudReadService;

    @Inject
    private transient CrudWriteService crudWriteService;
    
    private void readObject(ObjectInputStream inputStream) throws IOException, ClassNotFoundException {
        inputStream.defaultReadObject();
        crudWriteService =  Util.getBean(CrudWriteService.class);
        crudReadService = Util.getBean(CrudReadService.class, SUPER_ANNOTATION);
    }
    
    private static class SuperAnnotation extends AnnotationLiteral<SuperCrudBean> implements SuperCrudBean {}
    
    public static final SuperAnnotation SUPER_ANNOTATION = new SuperAnnotation();

    
    @Override
    public CrudReadService getCrudReadService() {
        return crudReadService;
    }

    @Override
    public CrudWriteService getCrudWriteService() {
        return crudWriteService;
    }

    public boolean isRememberTableState() {
        return super.isRememberTableState() && !FILTERING.equals(
                FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get(STATE));
    }
    private Set<String> sortingCols = new HashSet<>(5);

    @Override
    protected String newRowOnLastPage(String table) {
        return "lastPage('" + table + "');";
    }

    @Override
    protected String newRowOnCurrentPage(String table) {
        return "";
    }

    @Override
    public String gotoPageContaining(T entity) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    protected void fillCopy(T newEntity, T selected) {
    }

    @Override
    protected boolean useFilterMechanism() {
        return true;
    }
}
