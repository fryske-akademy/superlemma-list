/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fa.superlemmalist.jsf.converters;

import java.io.Serializable;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.faces.component.UIComponent;
import jakarta.faces.context.FacesContext;
import jakarta.faces.convert.Converter;
import jakarta.faces.convert.ConverterException;
import jakarta.faces.convert.FacesConverter;
import jakarta.inject.Named;
import org.fa.superlemmalist.jpa.LanguageVariant;
import org.fryske_akademy.jpa.Param;

/**
 *
 * @author eduard
 */
@FacesConverter(forClass = LanguageVariant.class, managed = true)
@Named
@ApplicationScoped
public class LanguageVariantConverter extends AbstractConverter implements Converter<LanguageVariant>, Serializable {

    @Override
    public LanguageVariant getAsObject(FacesContext facesContext, UIComponent component, String value) {
        if (value == null || value.length() == 0) {
            return null;
        }
        LanguageVariant lv = crudReadService.findOne("LanguageVariant.byCode", Param.one("code", value), LanguageVariant.class);
        if (lv == null) {
            throw new ConverterException(value + " no language");
        } else {
            return lv;
        }
    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent component, LanguageVariant o) {
        if (o == null) {
            return null;
        }
        return o.getCode();
    }
}
