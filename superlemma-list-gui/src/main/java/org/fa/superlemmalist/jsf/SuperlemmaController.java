package org.fa.superlemmalist.jsf;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import jakarta.annotation.PostConstruct;
import jakarta.enterprise.context.SessionScoped;
import jakarta.faces.annotation.ManagedProperty;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import org.fa.superlemmalist.jpa.Superlemma;
import org.fa.superlemmalist.jsf.converters.SuperlemmaConverter;
import org.fa.superlemmalist.jsf.lazy.LazySuperlemma;
import org.fa.tei.jaxb.v2_x.Join;
import org.fryske_akademy.jpa.Param;

@SessionScoped
@Named
public class SuperlemmaController extends SuperController<Superlemma, LazySuperlemma> {

    @Inject
    @ManagedProperty("#{lemmaController}")
    private LemmaController lemmaController;

    private final List<String> posCodes = new ArrayList<>(100);

    @PostConstruct
    private void init() {
        Arrays.stream(Join.Pos.values()).forEach(pos -> {
            posCodes.add("Pos."+pos.name().toUpperCase(Locale.ROOT));
        });
    }

    public String showLemmas(Superlemma s) {
        return filterAndRedirect(lemmaController, "superlemma", converter.getAsString(null, null, s), "/lemma/List");
    }

    @Inject
    @Named("superlemmaConverter")
    private SuperlemmaConverter converter;

    @Override
    public List<String> complete(String query) {
        List<String> l = new ArrayList<>(30);
        getCrudReadService().find("Superlemma.likeCode", Param.one("code", query), null, 30, Superlemma.class).forEach((h) -> {
            l.add(converter.getAsString(null, null, h));
        });
        return l;
    }

    @Override
    protected void fillCopy(Superlemma newEntity, Superlemma selected) {
        newEntity.setPos(selected.getPos());
    }

    public List<String> posCodes() {
        return posCodes;
    }

    @Override
    public Superlemma create(Superlemma e) {
        Superlemma t = super.create(e);
        getLazyModel().getFilters().put("code", t.getCode());
        return t;
    }
    
}
