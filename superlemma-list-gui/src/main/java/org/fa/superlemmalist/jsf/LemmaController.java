package org.fa.superlemmalist.jsf;

import java.util.ArrayList;
import java.util.List;
import jakarta.annotation.PostConstruct;
import jakarta.enterprise.context.SessionScoped;
import jakarta.enterprise.inject.Instance;
import jakarta.faces.annotation.ManagedProperty;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import org.fa.superlemmalist.jpa.LanguageVariant;
import org.fa.superlemmalist.jpa.Lemma;
import org.fa.superlemmalist.jpa.Superlemma;
import org.fa.superlemmalist.jsf.converters.LemmaConverter;
import org.fa.superlemmalist.jsf.converters.SuperlemmaConverter;
import org.fa.superlemmalist.jsf.lazy.LazyLemma;
import org.fryske_akademy.jpa.Param;

@SessionScoped
@Named
public class LemmaController extends SuperController<Lemma, LazyLemma> {

    @Override
    protected void fillCopy(Lemma newEntity, Lemma selected) {
        newEntity.setLanguageVariant(selected.getLanguageVariant());
        newEntity.setSuperlemma(selected.getSuperlemma());
    }
    
    private final List<LanguageVariant> languageVariants = new ArrayList<>(10);
    
    @PostConstruct
    private void init() {
        languageVariants.addAll(getCrudReadService().findAll(LanguageVariant.class));
    }
    
    @Inject
    @Named("superlemmaConverter")
    private SuperlemmaConverter superlemmaConverter;

    @Override
    protected void fillNew(Lemma entity) {
        if (getFilterString("superlemma")!=null) {
            entity.setSuperlemma( superlemmaConverter.getAsObject(null, null, getFilterString("superlemma")));
        }
    }

    
    @Override
    public List<String> complete(String query) {
        List<String> l = new ArrayList<>(30);
        getCrudReadService().find("Lemma.likeLemma", Param.one("lemma", query),null, 30, Lemma.class).forEach((h) -> {
            l.add(LemmaConverter.getKey(h));
        });
        return l;
    }
    
    @Override
    public Lemma create(Lemma e) {
        Lemma t = super.create(e);
        getLazyModel().getFilters().put("lemma", t.getLemma());
        return t;
    }

    public List<LanguageVariant> getLanguageVariants() {
        return languageVariants;
    }

    @Inject
    private Instance<SuperlemmaController> superlemmaController;

    public String showSuperlemma(Lemma l) {
        return filterAndRedirect(superlemmaController.get(), "code", l.getSuperlemma().getCode(), "/superlemma/List");
    }


}
