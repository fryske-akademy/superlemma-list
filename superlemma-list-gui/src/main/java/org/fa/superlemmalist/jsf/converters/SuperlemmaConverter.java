/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.fa.superlemmalist.jsf.converters;

import java.io.Serializable;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.faces.component.UIComponent;
import jakarta.faces.context.FacesContext;
import jakarta.faces.convert.Converter;
import jakarta.faces.convert.ConverterException;
import jakarta.faces.convert.FacesConverter;
import jakarta.inject.Named;
import org.fa.superlemmalist.jpa.Superlemma;
import org.fryske_akademy.services.CrudReadService;
import org.fryske_akademy.jpa.Param;

/**
 *
 * @author eduard
 */
@FacesConverter(forClass = Superlemma.class, managed = true)
@Named
@ApplicationScoped
public class SuperlemmaConverter extends AbstractConverter implements Converter<Superlemma>, Serializable {

    @Override
    public Superlemma getAsObject(FacesContext facesContext, UIComponent component, String value) {
        if (value == null || value.length() == 0) {
            return null;
        }
        return find(value, crudReadService);
    }

    public static Superlemma find(String value, CrudReadService service) {
        Superlemma rv = null;
        if (value.contains(":")) {
            String[] split = value.split(": ?", 3);
            if (split.length == 2) {
                rv = service.findOne("Superlemma.byCodeAndPos", new Param.Builder()
                        .add("code", split[0]).add("pos", split[1]).build(), Superlemma.class);
            } else {
                rv = service.findOne("Superlemma.byCodeAndPosAndMeaning", new Param.Builder()
                        .add("code", split[0]).add("pos", split[1]).add("meaning", split[2]).build(), Superlemma.class);
            }
        } else {
            rv = service.findOne("Superlemma.byCode", Param.one("code", value), Superlemma.class);
        }
        if (rv == null) {
            throw new ConverterException(value + " not a superlemma");
        } else {
            return rv;
        }
    }

    public static String getKey(Superlemma o) {
        String s = o.getCode();
        if (o.getPos() != null) {
            s += ": " + o.getPos();
            if (o.getMeaning() != null) {
                s += ": " + o.getMeaning();
            }
        } else if (o.getMeaning() != null) {
            throw new ConverterException("meaning without linguistics in superlemma not supported: " + o);
        }
        return s;

    }

    @Override
    public String getAsString(FacesContext facesContext, UIComponent component, Superlemma object) {
        if (object == null) {
            return null;
        }
        return getKey(object);
    }
}
