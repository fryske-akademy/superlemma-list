package org.fa.superlemmalist.jsf;

import java.util.ArrayList;
import java.util.List;
import jakarta.enterprise.context.SessionScoped;
import jakarta.faces.annotation.ManagedProperty;
import jakarta.inject.Inject;
import jakarta.inject.Named;
import org.fa.superlemmalist.jpa.LanguageVariant;
import org.fa.superlemmalist.jsf.converters.LanguageVariantConverter;
import org.fa.superlemmalist.jsf.lazy.LazyLanguageVariant;
import org.fryske_akademy.jpa.Param;

@SessionScoped
@Named
public class LanguageVariantController extends SuperController<LanguageVariant, LazyLanguageVariant> {

    @Inject
    @ManagedProperty("#{lemmaController}")
    private LemmaController lemmaController;

    public LemmaController getLemmaController() {
        return lemmaController;
    }

    public void setLemmaController(LemmaController lemmaController) {
        this.lemmaController = lemmaController;
    }

    @Inject
    @Named("languageVariantConverter")
    private LanguageVariantConverter converter;

    public String showLemmas(LanguageVariant s) {
        return filterAndRedirect(lemmaController, "languageVariant", converter.getAsString(null, null, s), "/lemma/List");
    }


    public List<String> complete(String query) {
        List<String> l = new ArrayList<>(30);
        getCrudReadService().find("LanguageVariant.likeCode", Param.one("code", query), null, 30, LanguageVariant.class).forEach((h) -> {
            l.add(h.getCode());
        });
        return l;
    }

}
