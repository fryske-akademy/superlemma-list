## linking language information in various systems at the lemma level

This repository describes the superlemma list. It is a database that can be used to loosely couple / link language information in various separately managed systems such as corpora, dictionaries and lexicons. The information in the superlemma list can be managed automatically from other (lexicon) systems via a library, direct management is available as well. The superlemma list has a mechanism that allows linking systems to deal with changes, see [changes.md](changes.md).
It is a EE 8 / jsf 2.3 application that uses primefaces and includes web/rest service interfaces.

The superlemma list API can be found [here](API.md)

The datamodel consists out of three tables (TODO consider removing code from superlemma) (see also [sql](datamodel.sql)):

![datamodel](datamodel.png)

Example data:

![example data](example%20data.png)

The table superlemma contains linguistics (see https://bitbucket.org/fryske-akademy/tei-encoding) and meaning to distinguish homonyms.

### Basic usage
An application containing lemma information in a language can query the superlemma list, for this the superlemma list provides functions in an [API](API.md)  
Application can expose query functions that can be called with information from the superlemmalist.

When loosely linking systems it is crucial to keep the systems in sync. In [changes.md](changes.md) the strategy to deal with changes is described.
